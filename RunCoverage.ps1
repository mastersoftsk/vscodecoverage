﻿param(
    [Parameter(Mandatory=$true)]
    [string]$SolutionDirectory = "c:\Users\pavol\source\repos\Planner\",
    [Parameter(Mandatory=$true)]
    [string]$ProjectName="Staffingboard",
    [Parameter(Mandatory=$true)]
    [string]$TestProjectName="Staffingboard.UT",
    [string]$CoverageDirectory="CodeCoverage",
    [string]$OpenCover,
    [string]$ReportGenerator,
    [string]$Nunit
)

function FindTool([string] $FileName)
{
    Get-ChildItem -Path $ToolsDirectory -Include $FileName -File -Recurse -ErrorAction SilentlyContinue | Select-Object -First 1
}

$CoverageRunDirectory = Get-Date -Format yyyy_MM_dd_HH_mm
Write-Output "Results will be stored in directory $CoverageRunDirectory"

$ResultsDirectory=[System.IO.Path]::Combine($SolutionDirectory, $TestProjectName, $CoverageDirectory, "results")
$HistoryDirectory=[System.IO.Path]::Combine($SolutionDirectory, $TestProjectName, $CoverageDirectory, "history")
$CoverageRunPath = [System.IO.Path]::Combine($ResultsDirectory, $CoverageRunDirectory)
$ToolsDirectory = [System.IO.Path]::Combine($SolutionDirectory, "packages")

$ProjectDirectory = [System.IO.Path]::Combine($SolutionDirectory, $ProjectName)
$TestProjectDirectory = [System.IO.Path]::Combine($SolutionDirectory, $TestProjectName)

Write-Output "Creating output directory $CoverageRunPath"
New-Item $CoverageRunPath -ItemType directory -Force

Write-Output "`r`nTools used:"
if([string]::IsNullOrEmpty($OpenCover))
{
    $OpenCover = FindTool("OpenCover.Console.exe")
}

Write-Output "    OpenCover: $OpenCover"

if([string]::IsNullOrEmpty($ReportGenerator))
{
    $ReportGenerator = FindTool("ReportGenerator.exe")
}

Write-Output "    ReportGenerator: $ReportGenerator"


if([string]::IsNullOrEmpty($Nunit))
{
    $Nunit = FindTool("nunit3-console.exe")
}

Write-Output "    NUnit: $Nunit"

Write-Output "`r`nRunning OpenCover"

Write-Output "$OpenCover -target:$Nunit -targetargs:/result=$ResultsDirectory\$ProjectName.xml $TestProjectDirectory\bin\Debug\$TestProjectName.dll -output:$CoverageRunPath -filter:+[*]* -[*$TestProjectName]* -register:user"
&$OpenCover -target:$Nunit -targetargs:"/result=$ResultsDirectory\$ProjectName.xml $TestProjectDirectory\bin\Debug\$TestProjectName.dll" -output:"$CoverageRunPath\coverage_results.xml" "-filter:+[*]* -[*$TestProjectName]*" -register:user


Write-Output "`r`nRunning ReportGenerator"
&$ReportGenerator "-reports:$CoverageRunPath\coverage_results.xml" "-targetdir:$CoverageRunPath" "-historydir:$HistoryDirectory" "-filters:-$TestProjectName" -reporttypes:Html "-sourcedirs:$ProjectDirectory\"

Invoke-Item $CoverageRunPath\index.htm



