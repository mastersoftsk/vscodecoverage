﻿param(
	[Parameter(Mandatory=$true)]
    [string]$Project = "c:\Users\pavol\source\repos\Planner\Staffingboard.UT\Staffingboard.UT.csproj"
            
)

write-output "   Enabling code coverage for project $Project" 
Push-Location

$fileExists = Test-Path -Path $Project
if($fileExists -eq $FALSE) 
{
	write-output "ERROR: The file $Project does not exist!"    
    exit
}

$projectDirectory =  Split-Path -Parent $Project
$solutionDirectory = Split-Path -Parent $projectDirectory

write-output "   Project directory: $projectDirectory"
write-output "   Solution directory: $solutionDirectory"

$projectFileName = Split-Path -Leaf $Project
$projectName = [System.IO.Path]::GetFileNameWithoutExtension($projectFileName)
write-output "   Project name: $projectName"

function InstallPackage([string] $PackageName)
{
    Try
    {        
        nuget.exe install $PackageName -SolutionDirectory $solutionDirectory -Verbosity quiet
        nuget.exe list $PackageName -Source "$solutionDirectory\packages"                
    }
    Catch
    {
        write-output "$_.Exception"
    }
}

cd $projectDirectory

write-output "   Installing package 'nunit.console' for solution $solutionDirectory"
$nunit = InstallPackage("nunit.console")
write-output "   NUnit.Console version installed: $nunit"

write-output "   Installing package 'OpenCover' for solution $solutionDirectory"
$openCover = InstallPackage("OpenCover")
write-output "   OpenCover version installed: $openCover"

write-output "   Installing package 'ReportGenerator' for solution $solutionDirectory"
$reportGenerator = InstallPackage("ReportGenerator")
write-output "   ReportGenerator version installed: $reportGenerator"

Pop-Location